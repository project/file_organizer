If your site has a huge number of files in the root `public://` directory, this can help. (This does not offer the option to apply to the `private://` directory yet. ). This is especially useful if you have switched to s3 and use a file WYSIWYG file management tool like IMCE.

The module has no user interface. It is just a drush command `drush file-organizer` with some additional functions including identifying a subdirectory in public://, getting feedback on what is being moved. Eventually the goal is to also allow it to search and replace file patterns in any of a site's wysiwyg fields where a file might have been inserted into text. Currently it only works on the `field_body` field.

This module is not ready for live site use unless you have ready backups of both the files directory and the database.

How it works:

* It searches the file_managed table for any files that reside in the root (or the specified subdirectory).
* With the list of files, it loops through each and, using the timestamp of the file, moves the file to a subdirectory based on the year and month. (If a file was created in January of 2018, then the file will be moved to `public://2018/09/`.
* The system then updates the file_managed table with the new file location
* Then searches the `field_body` field for any reference to the filename and updates its path.
* At the end of the process you will see a list of errors if any.

This has not been tested with the Media module.

Options for the command:
--feedback option it will report each file that will be moved and confirm that it was moved.
--dir=[some-directory] will process files in a given directory (for example if you have a subdirectory called jumbotron_images, then you'd write --dir=jumbotron_images.

Future updates will include the following option:
--fields=[comma,separated,list,of,fields] will not only include the field_body but other fields you submit to check for the files and update any references to them. So if you also have a field called field_sidebar, and field_highlight, you'd enter --fields=field_sidebar,field_highlight.
