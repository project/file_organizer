<?php


/**
 * @return mixed
 */
function file_organizer_drush_command()
{
  $items['file-organizer'] = [
    'description' => 'Move all known files from the root of the file system to directories organized by Year/month according to the file_managed timestamp. Be sure to run a backup of your site first!',
    'options' => [
      'dir' => 'Enter the sub-directory you want to target. Assumes the root of the public directory',
      'feedback' => 'Show the files being moved. Assumes false',
    ],
  ];
  return $items;
}


/**
 * @throws \Exception
 */
function drush_file_organizer()
{
  // if no dir option is provided, we will just focus on the root directory.
  $dir = drush_get_option('dir', FALSE);
  $feedback = drush_get_option('feedback', FALSE);

  // get all the text_long fields in the database.
  $field_query = db_select('field_config', 'fc')
    ->condition('type', 'text_long')
    ->fields('fc', ['field_name']);
  $fields = $field_query->execute()->fetchCol();
  $fields[] = 'body';
  $field_list = [];
  foreach ($fields as $field) {
    $field_list[] = (object)[
      'name' => $field,
      'table' => 'field_data_' . $field,
      'column' => $field . '_value',
    ];
  }

  $query = db_select('file_managed', 'fm');
  if ($dir) {
    // in the target directory
    $query->condition('uri', 'public://' . $dir . '/%', 'LIKE');
    // but not in any directories in the target directory
    $query->condition('uri', 'public://' . $dir . '/%/%', 'NOT LIKE');
  } else {
    $query->condition('uri', 'public://%/%', 'NOT LIKE');
  }
  $query->fields('fm');
  $result = $query->execute();
  if ($feedback) {
    drush_print(dt('Will move @count files.', [
      '@count' => $result->rowCount(),
    ]));
  }
  if (drush_confirm('Make sure you run a backup before you complete this! Are you ready to continue?')) {
    drush_print('Here we go!');
  } else {
    return drush_user_abort();
  }

  foreach ($result as $item) {
    $source_file = $item->filename;
    if ($dir) {
      $source_uri = 'public://' . $dir . '/' . $source_file;
    } else {
      $source_uri = 'public://' . $source_file;
    }

    // new directory is based on the date of the file.
    $destination_dir = date('Y/m/', $item->timestamp);

    if ($dir) {
      $destination_dir = $dir . '/' . $destination_dir;
    }
    $destination_file = $destination_dir . transliteration_clean_filename($source_file);
    $uri = 'public://' . $destination_file;
    $directory_uri = 'public://' . $destination_dir;

    // prepare an update of the file record to point it to the $destination_dir
    // if the file move doesn't work the entire db update will fail. This is the most likely point of failure.
    // build a string of exceptions to report at the end of the process
    $exceptions = [];
    try {
      //check if the directory is there, if not create it.
      if (!file_prepare_directory($directory_uri, FILE_CREATE_DIRECTORY)) {
        drupal_mkdir($directory_uri, NULL, TRUE);
      }

      file_unmanaged_move($source_uri, $uri, FILE_EXISTS_REPLACE);

      $update_query = db_update('file_managed')
        ->condition('fid', $item->fid)
        ->fields([
          'uri' => $uri,
        ]);
      $update_query->execute();
      //search for all $fields for the $source_file and collect a list of entity_ids.

      $encoded_file_name = rawurlencode($source_file);
      foreach ($field_list as $field) {

        $field_search = db_select($field->table, 'fb');
        $field_search->condition('fb.' . $field->column, '%' . $encoded_file_name . '%', 'LIKE');
        $field_search->fields('fb');
        $records = $field_search->execute();
        foreach ($records as $record) {
          // update node.
          $node = node_load($record->entity_id);
          $body_value = $node->{$field->name}[LANGUAGE_NONE][0]['value'];
          $new_value = str_replace($encoded_file_name, $destination_file, $body_value);
          $node->{$field->name}[LANGUAGE_NONE][0]['value'] = $new_value;
          node_save($node);
        }
      }
      // https://www.drupal.org/project/file_organizer/issues/3103236 add block_custom to the search process.
      $block_search = db_select('block_custom', 'bc')
        ->condition('body', '%' . $encoded_file_name . '%', 'LIKE')
        ->fields('bc');
      $blocks = $block_search->execute();
      foreach ($blocks as $block) {
        $new_value = str_replace($encoded_file_name, $destination_file, $block->body);
        db_update('block_custom')
          ->condition('bid', $block->bid)
          ->fields(['body' => $new_value])
          ->execute();
      }
      if ($feedback) {
        drush_print(dt('Moved @source to @destination.', [
          '@source' => $source_uri,
          '@destination' => $uri,
        ]));
      }
    } catch (\Exception $e) {
      $exceptions[] = $e->getMessage();
    }
  }
  $count = 0;
  if (!empty($exceptions)) {
    drush_print('The following errors occurred:');
    foreach ($exceptions as $exception) {
      drush_print($exception);
      $count++;
    }
  }
  drush_print(dt('There were @count errors in the process', ['@count' => $count]));


}
